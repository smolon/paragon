//Adam Solon
let prodcuts;
if(!localStorage.getItem("products")){
    products = [{
        name: "Jabłka",
        ilosc: "1.5",
        cena: "4.9"
    },
    {
        name: "Bułka",
        ilosc: "5",
        cena: "0.49"
    }
];
window.localStorage.setItem('products', JSON.stringify(products));
}
else {
    products = JSON.parse(localStorage.getItem("products"));
}

let lastIndex = 1;
let allSum = 0;
function generateTable(table, data) {
    let i = 3;
    for (let element of data) {
        i++;
        let tempIndex = data.indexOf(element) + 1;
        let row = table.insertRow();
        let cell = row.insertCell();
        let index = document.createTextNode(tempIndex)
        cell.appendChild(index);
        cell.setAttribute("class", "lp");
        cell.setAttribute("id",tempIndex);
        for (key in element) {
            let cell = row.insertCell();
            let text;
            if(key == "cena"){
            text = document.createTextNode(element[key] + " zł");
            }else {
            text = document.createTextNode(element[key]);
            }
            cell.appendChild(text);
            if (key == "name") {
                cell.setAttribute("class", "nazwa");
            }
        }
        let cell2 = row.insertCell();
        let countSum = parseFloat((element.ilosc * element.cena).toFixed(2));
        let rowSum = document.createTextNode(countSum + " zł");
        allSum += countSum;
        cell2.appendChild(rowSum);
        let btn1 = document.createElement("button");
        btn1.innerHTML = "Usuń"
        btn1.setAttribute("class", "btn btn-danger");
        let cell3 = row.insertCell();
        cell3.appendChild(btn1);
        let btn2 = document.createElement("button");
        btn2.innerHTML = "Edytuj"
        btn2.setAttribute("class", "btn btn-secondary");
        btn1.setAttribute("onclick", "deleteProduct(this)");
        btn2.setAttribute("onclick", "editProduct(this)")
        let cell4 = row.insertCell();
        cell4.appendChild(btn2);
    }
    let row = table.insertRow();
    let cell1 = row.insertCell();
    let text = document.createTextNode("RAZEM");
    cell1.appendChild(text);
    cell1.setAttribute("colspan", "4");
    cell1.setAttribute("class", "razem");
    let cell2 = row.insertCell();
    let dispsum = document.createTextNode(allSum.toFixed(2) + " zł");
    cell2.appendChild(dispsum);
    cell2.setAttribute("class", "razem");
    let cell3 = row.insertCell();
    cell3.setAttribute("colspan", "2");
    cell3.setAttribute("class", "razem");
    $('tbody').attr("id", "products");
    $('#products').sortable({
        items: 'tr:not(:last)',
        start: function(e, ui) {
            $(this).attr('data-previndex', ui.item.index());
        },
        update: function(e, ui) {
            var newIndex = parseInt(ui.item.index())+1;
            var oldIndex = parseInt($(this).attr('data-previndex'))+1;
            var element_id = ui.item.attr('id');
            $(this).removeAttr('data-previndex');
            array_move(products, oldIndex-1, newIndex-1);
            location.reload();
            window.localStorage.setItem("products", JSON.stringify(products));
        },
        axis: 'y'
    });
    $('#prodcuts').disableSelection();
}
function array_move(arr, old_index, new_index) {
    while (old_index < 0) {
        old_index += arr.length;
    }
    while (new_index < 0) {
        new_index += arr.length;
    }
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; 
};
function generateTableHead(table) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    let th1 = document.createElement("th");
    let th2 = document.createElement("th");
    let th3 = document.createElement("th");
    let th4 = document.createElement("th");
    let th5 = document.createElement("th");
    let th6 = document.createElement("th");
    let lp = document.createTextNode("LP");
    let name = document.createTextNode("NAZWA");
    let count = document.createTextNode("ILOŚĆ");
    let price = document.createTextNode("CENA");
    let sum = document.createTextNode("SUMA");
    th1.appendChild(lp);
    th2.appendChild(name);
    th3.appendChild(count);
    th4.appendChild(price);
    th5.appendChild(sum);
    row.appendChild(th1);
    row.appendChild(th2);
    row.appendChild(th3);
    row.appendChild(th4);
    row.appendChild(th5);
    row.appendChild(th6);
    th6.setAttribute("colspan", "2");
    th1.setAttribute("class", "lp");
    th2.setAttribute("class", "nazwa");
}
let table = document.querySelector("table");
table.setAttribute("class", "products");
generateTable(table, products);
generateTableHead(table);
(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    };
}(jQuery));
$("#name").inputFilter(function(value) {
    return /^[a-z\u00c0-\u024f]*$/i.test(value);
});
$("#price, #count").inputFilter(function(value) {
    return /^\d*[.]?\d*$/.test(value);
});

function checkIfEmpty() {
    let name = document.getElementById('name').value;
    let count = document.getElementById('count').value;
    let price = document.getElementById('price').value;
    let sum = parseFloat(count) * parseFloat(price);
    if (name.textLength != 0) {
        if (count.textLength != 0) {
            if (price.textLength != 0) {
                subnewproduct.disabled = false;
            } else {
                subnewproduct.disabled = true;
            }
        } else {
            subnewproduct.disabled = true;
        }

    } else {
        subnewproduct.disabled = true;
    }
}
let checkIndex = 0;

function checkName() {
    if (this.textLength >= 2) checkIndex++;
    if (checkIndex == 3) submitButton.disabled = false;
}

function checkCount() {
    if (this.textLength >= 2) checkIndex++;
    if (checkIndex == 3) submitButton.disabled = false;
}

function checkPrice() {
    if (this.textLength >= 2) checkIndex++;
    if (checkIndex == 3) submitButton.disabled = false;
}

function checkform() {
    if (document.getElementById('name').value == "") {
        alert("asd");
        return false;
    }
    return true;
}

function addProduct() {
    let name = document.getElementById('name').value;
    let count = document.getElementById('count').value;
    let price = document.getElementById('price').value;
    let sum = parseFloat(count) * parseFloat(price);
    if (name == "" || price == "" || count == "") {
        return;
    }
    products.push({
        name: name,
        ilosc: count,
        cena: price
    });
    window.localStorage.setItem("products", JSON.stringify(products));
    $(".products tbody tr:last").remove();
    let markup1 = '<tr><td class="lp">' + (lastIndex++) + '</td><td class="nazwa">' +
        name + '</td><td>' + count + '</td><td>' + price + '</td><td>' + sum.toFixed(2) +
        '</td><td><button type="button" class="btn btn-danger">Usuń</td><td><button type="button" class="btn btn-secondary">Edytuj</td><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span></tr>';
    $(".products tbody").append(markup1);
    let markup2 = '<tr><td colspan="4" class="razem">RAZEM</td><td class="razem">' +
        (allSum + sum).toFixed(2) + ' zł</td><td colspan="2" class="razem"></td></tr>';
    allSum = allSum + sum;
    $(".products tbody").append(markup2);
}
let editRow;
let ti;

function deleteProduct(ctl) {
    let rows2 = $("tr", table);
    let cols2 = rows2.children("td");
    let rows = $(ctl).parents("tr");
    let cols = rows.children("td");
    $(".lp").val($(cols[21]).text());
    let delIndex = $(rows).index() + 1
    products.splice(delIndex - 1, 1);
    window.localStorage.setItem('products', JSON.stringify(products));
    let asd = 123123;
    $(ctl).parents("tr").remove();
    location.reload();
    for (let i = delIndex; i < rows2.length - 1; i++) {
        let lp = rows.find("tr:eq(0)").find("td:eq(0)").text();
    }
}

function productUpdateInTable() {
    $(editRow).after(productBuildTableRow());
    $(editRow).remove();
    formClear();
}

function productBuildTableRow() {
    let name = $("#name").val();
    let count = $("#count").val();
    let price = $("#price").val();
    let sum = parseFloat(count) * parseFloat(price);
    let row = '<tr><td class="lp">' + ti + '</td><td class="nazwa">' +
        name + '</td><td>' + count + '</td><td>' + price + '</td><td>' + sum +
        '</td><td><button type="button" class="btn btn-danger">Usuń</td><td><button type="button" class="btn btn-secondary">Edytuj</td></tr>';
    return row;
}

function editProduct(ctl) {
    let row = $(ctl).parents("tr");
    editRow = $(ctl).parents("tr");
    let editIndex = $(row).index();
    let cols = row.children("td");
    let test = $(cols[3]).text();
    ti = $(cols[0]).text();
    $("#name").val($(cols[1]).text());
    $("#count").val($(cols[2]).text());
    $("#price").val(parseFloat($(cols[3]).text(),10));
    $("#form-header").text("Aktualizacja pozycji");
    $("#submitButton").attr("value", "Zaktualizuj");
    $("#submitButton").removeAttr("onclick");
    $("#submitButton").click(function() {
        products[editIndex].name = $("#name").val();
        products[editIndex].ilosc = $("#count").val();
        products[editIndex].cena = $("#price").val();
        window.localStorage.setItem('products', JSON.stringify(products));
    });
}

function formClear() {
    $("#name").val("");
    $("#count").val("");
    $("#price").val("");
}